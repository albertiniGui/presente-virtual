import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MensagensComponent } from './mensagens/mensagens.component';
import { CardHorizontalComponent } from './modules/card-horizontal/card-horizontal.component';
import { CardPadraoComponent } from './modules/card-padrao/card-padrao.component';
import { CardGrandeCimaComponent } from './modules/card-grande-cima/card-grande-cima.component';
import { CardGrandeBaixoComponent } from './modules/card-grande-baixo/card-grande-baixo.component';
import { CardGroupComponent } from './modules/card-group/card-group.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MensagensComponent,
    CardHorizontalComponent,
    CardPadraoComponent,
    CardGrandeCimaComponent,
    CardGrandeBaixoComponent,
    CardGroupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
