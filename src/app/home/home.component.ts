import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  public isMobile: boolean;

  public isModal = false;
  public isTexto1 = true;
  public isTexto2 = false;

  ngOnInit() {
    this.onResize();
    this.verificaMobile();
  }

  onResize() {
    $(window).resize(function () {
      this.verificaMobile();
    })
  }

  verificaMobile() {
    if ($(window).width() < 1000) {
      this.isMobile = true;
      $(".div-bem-vinda").css("background-repeat", "round")
      $(".div-bem-vinda").css("background-image", `url("../../assets/imagens/background.png")`);
    }
    else {
      this.isMobile = false;
      $(".div-bem-vinda").css("background-repeat", "no-repeat");
      $(".div-bem-vinda").css("background-image", `url("../../assets/imagens/backgroundpc.png")`);
    }
  }

  abrirModal() {
    this.isModal = true;
    $(".modal-back").removeClass("opacidade-0");
  }

  trocaTexto() {
    $(".modal-front.modal-1").addClass("opacidade-0");
    setTimeout(() => {
      this.isTexto1 = false;
      this.isTexto2 = true;
    }, 400)
  }

  fecharModal(posicao?) {
    $(".modal-back").addClass("opacidade-0");
    setTimeout(() => {
      $(".modal-back").hide();
      this.isModal = false;
      if (posicao) {
        $(".div-bem-vinda").addClass("opacidade-0");
        setTimeout(() => {
          this.router.navigate(["mensagens"]);
        }, 400)
      }
    }, 400)
  }

}
