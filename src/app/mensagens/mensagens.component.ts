import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-mensagens',
  templateUrl: './mensagens.component.html',
  styleUrls: ['./mensagens.component.css']
})
export class MensagensComponent implements OnInit {

  constructor() { }

  public isMobile: boolean;
  public isLoaded: boolean;
  public isFinal: boolean;

  public contagemUnicornio: number = 0;

  public backgrounds_fundo = [];

  public musicas = ["best-day", "bring-it", "watermelon-sugar", "gone", "ho-hey", "home", "lights-up"];
  public musica;

  public guilherme = {
    imagem: 'gui-elen.jpg',
    titulo: 'Oi gatinha',
    texto: 'Oh meu deus, que sacanagem fazermos um isolamento justo na época do seu aniversário. Fiquei muito triste de saber que não íamos poder comemorar essa data juntos, lado a lado na vida real. Por outro lado, pensei que a hora de fazer um presente desses era essa! Fiz com todo o amor e carinho que pude, com a ajuda de todos os escritores dessas mensagens que você leu! Saiba que a gente te ama muito, mas muito, mas muito mesmo, não tem noção de quanto. E pra você, meu amor, eu desejo tudo que a vida pode proporcionar de bom. Tudo que o universo consiga manipular para te arrancar um sorrisinho. Tudo que eu consiga fazer pra animar seu dia nem que for um teco de nada. Estamos longe agora, mas espero que haja um quentinho no seu coração, e que eu (e o pessoal que escreveu também, vai) seja responsável por ele. Vou repetir por que não canso de falar isso: Te amo. Te amo 3.198.760 de 3.198.760. Espero ansioso a comemoração tardia desse niver, lindona.',
    autor: 'Guilherme Albertini Clemente',
  }

  public barbara = {
    imagem: 'barbara.jpeg',
    titulo: 'Oi de novo irmã!',
    texto: 'Hoje eu queria estar assim, bêbada, abraçada com a minha alma gêmea esperando para ver o pôr do sol – as 6 da manhã. Mas esse tempo a gente recupera mais pra frente, já que, pelo que eu conheço de nós, vai ser a primeira coisa que a gente vai fazer no fim dessa quarentena – depois, é claro, de comer um Rox e ver um filminho. Desejo pra você toda a felicidade do mundo, seus sonhos mais impossíveis se realizando. O mundo inteiro é seu, e se quiser outro planeta, eu arrumo um jeito de te dar. ❤️',
    autor: 'Babi Viotto',
  }

  public mae_pai_babi = {
    imagem: "elen.jpg",
    titulo: "Oi Elen!",
    texto: "Gostamos muito de você e ficamos muito felizes em ver suas realizações! Tenha um ótimo ano, com muita felicidade. Parabéns!",
    autor: "Rita e Moacir"
  }

  public fernandes = {
    imagem: 'fernandes.jpeg',
    titulo: 'Oi amiga!',
    texto: 'Oie miga hehe, cara é muito louco pensar que estamos comemorando os 20 anos todas juntas, e comemorando essa mesma data á 9 anos caraaaa. Você tendo um alto cargo de uma das pessoas mais importantes da minha vida, agradeço  todos os dias por ter uma amiga como você, esse serzinho cheio de luz que sempre iluminou vários caminhos quando eu estava precisando e eu só posso desejar pra ti o melhor que a vida e o mundo tem a oferecer, porque você merece tudo e um pouco mais. Amo você e assim que essas coisas tudo acabarem espero poder comemorar sua vida e te dar um abração. Obrigada por ser você e por estar na minha vida ❤️❤️',
    autor: 'Bia Fernandes',
  }

  public leao = {
    imagem: 'leao.jpeg',
    titulo: 'Alô meu amor!',
    texto: 'Muitos aniversários depois eu já não sei o que escrever! Foram tantos textinho, tantas fotos postadas, tantos stories e talvez eu tenha perdido um pouco da criatividade. Hahaha enfim... Parabéns coisa linda, muitos anos de vida, felicidades, saúde(tudo aquilo que o povo fala), tudo de melhor sempre! To com saudades de você e do seu mingau matinal! Te amo🥰❤️ Conta comigo pro que precisar sempre!!!! Obs: aguardo role pós quarentena🙃',
    autor: 'Bia Leão',
  }

  public isabelle = {
    imagem: 'isabelle.jpeg',
    titulo: 'Oiêêê',
    texto: 'Parabéns Elen, te desejo tudo de bom, muitas felicidades, saúde (principalmente nesse momento horrível que estamos passando) e muitos aninhos de vida para continuar nos alegrani com tua presença tão cheia de luz, de carinho, de energia!! TE AMO MUITO!!! Feliz Aniversário e muito obrigada por sua amizade e por ser essa pessoa tão especial!',
    autor: 'Belão',
  }

  public vitor = {
    imagem: 'vitor.jpeg',
    titulo: 'Fala minha querida',
    texto: 'Elen tu é top e feliz aniversário e mts felicidades.',
    autor: 'Vitim',
  }

  public matheus = {
    imagem: 'matheus.jpg',
    titulo: 'Fala Elen!',
    texto: 'Parabensssssssssssssssssssssssssss cachorra belga, ta ficando velha em?! Então, eu não tenho muito o que dizer porque no meu ponto de vista a única coisa que tu fez para mim até hoje é me deixar bem puto, por que? Ué, se roubou meu Gui mano, meu Fox pô. Ta, parei hehe, parabéns Elen, muitos anos de vida, e todos esse clichês que você vai ler e ouvir nessa data tão querida, muito obrigado por fazer o meu irmão feliz, obrigado por ter feito parte da minha festa surpresa e ter feito aquele brigadeiro incrível, não somos nem um pouco próximos e não existe muitas histórias para contar, mas só de saber que você faz meu irmão tão feliz, sei que você é uma pessoa incrível, muito amada e tem um coração bom, então espero que você esteja aproveitando seu dia. Feliz aniversário, eh nois.',
    autor: 'Matheus Moller',
  }

  public natalia = {
    imagem: 'nathalia.jpeg',
    titulo: 'Olááá Eleeeen!',
    texto: 'Bom, com o texto ao lado já deu para perceber que o Matheus é um c*zão não é mesmo? Mas não é surpresa para ninguém, você até o conhece mais tempo que eu, então... sem novidades. Sim, eu sei, ele não é o assunto do momento, mas eu tive que fazer um breve comentário, i´m sorry. E ai curtiu a surpresinha marota do nosso querido Fox, mais conhecido como amor da sua vida? Confesso que eu fiquei eufórica quando soube, QUE CRITIVIDADE, QUE MENINO, e eu nem sei como isso vai ficar, mas estou ansiosa. Tudo isso só comprova o quanto você é amada e especial, acho que chegamos ao assunto principal. Elen Louise Marra, futura arquiteta, amante do universo, uma inspiração, a própria obra de arte em forma de mulher! Hoje o dia é todinho seu, assim como os outros, porém é hoje que você fecha e inicia mais um ciclo, mais uma década, mais um outono, mais uma jornada, então mesmo não tendo tanta intimidade e nem sendo tão próximas quero te desejar todo sucesso e felicidade do mundo, muita luz, que você possa continuar buscando sua melhor versão e sendo melhor a cada dia, não só para as pessoas a sua volta, mas para você mesma. Feliz aniversário Elen!',
    autor: 'Nathy',
  }

  public neuza = {
    imagem: 'neuza.jpeg',
    titulo: 'Almoço de domingo na vó ❤️🙌',
    texto: 'Parabéns Elen! Fico muito feliz de ter você na nossa família! Que deus abençoe muito sua vida e que você tenha muito sucesso! Que você e o Gui ainda tenham muito tempo para se amarem. Beijos.',
    autor: 'Vó Neuza, Vô Luíz e Marquinho',
  }

  public ciaei = {
    imagem: 'ciaei.jpeg',
    titulo: '',
    texto: 'Aproveitando um momento de cultura!',
    autor: 'Sônia Albertini',
  }

  public pastel = {
    imagem: 'pastel.jpeg',
    titulo: '',
    texto: 'Comendo um pastel em família.',
    autor: 'Sônia Albertini',
  }

  public ester = {
    imagem: 'ester.jpeg',
    titulo: 'Parabéeens Elen!',
    texto: 'Elen linnnnda!! Nossa vida com você é mais completa!! Te amo ❤️ feliz aniversário ❤️',
    autor: 'Ester, Paulinho e Matheus',
  }

  public cores = {
    imagem: 'cores.jpeg',
    titulo: '',
    texto: 'Que dia mais lindo e cheio de cores!.',
    autor: 'Sônia Albertini',
  }

  public bruna = {
    imagem: 'bruna.jpeg',
    titulo: 'Faaaaala Cunhaa',
    texto: 'Aeeeee cunha, feliz niver! Muitos anos de vida! Que venham muitos aniversários para comemorarmos todos juntos! Que você realize seus objetivos e sonhos, e tenha uma vida leve e bonita, com muita saúde e paz sempre. Você é luz. Muito amor na sua vida é oq desejo ❤️️ parabéns!',
    autor: 'Bruna Albertini',
  }

  public doida = {
    imagem: 'doida.jpeg',
    titulo: '',
    texto: "Uma família quase, mas quase normal. Rock'n'Roll na veia KKKKKKKKKKKK",
    autor: 'Sônia Albertini',
  }

  public normal = {
    imagem: 'normal.jpeg',
    titulo: '',
    texto: 'Agora sim uma família normal. Jóinha pra não perder a graça!',
    autor: 'Sônia Albertini',
  }

  public casal = {
    imagem: 'casal.jpeg',
    titulo: '',
    texto: 'Que casal marrrrrrrr lindo!',
    autor: 'Sônia Albertini',
  }

  public panela = {
    imagem: 'panela.jpeg',
    titulo: '',
    texto: 'Unidos na panela! Sempre cozinhando nessa casa, desde strogonoff até um brigadeirozinho.',
    autor: 'Bruna Albertini',
  }

  public tania = {
    imagem: 'tania.jpeg',
    titulo: 'Oi Eleeen!',
    texto: 'Feliz aniversário!!!🎉🎂🎁 Desejamos que seu dia seja tranquilo(!) , alegre, cheio de expectativas e projetos. É isso que precisamos todos! Projetos! E um foco enorme para realizar. Muita paz, saúde e dindim também.',
    autor: 'Beijos, Tania , Beto, Duda e Ben.',
  }

  public marcos = {
    imagem: 'marcos.jpeg',
    titulo: 'Olá Elen!',
    texto: 'Parabéns minha norinha querida, te desejo toda felicidade do mundo, muita saúde, muita paz, muita paciência e prudência para passarmos essa faze que o mundo inteiro está passando, logo todos nós estaremos juntos se abraçando e confreternizando. Bjos.',
    autor: 'Marcos Clemente',
  }

  public nowah = {
    imagem: 'nowah.jpg',
    titulo: 'Awaw..',
    texto: 'Awawaw, awaw. Awawaw awawawaw ❤️🐶',
    autor: 'Nowah Maria',
  }

  public eliane = {
    imagem: 'eliane.jpeg',
    titulo: 'Oi Filha!',
    texto: 'De repente 20... meu bebê cresceu e se tornou uma mulher determinada que batalha pelo que quer. Tenho muito orgulho de vc. Feliz aniversário 🎂🎈 Deus te abençoe e te guarde sempre🙏🏼 TE AMO. Parabéns 👋👋🎈🎉',
    autor: 'Eliane',
  }

  public eliane2 = {
    imagem: 'eliane2.jpeg',
    titulo: '',
    texto: 'Feliz aniversário 🎂🎈 pra minha decoradora/Masterchef/arquiteta preferida❤️❤️',
    autor: 'Eliane',
  }

  public fernando = {
    imagem: 'fernando.jpeg',
    titulo: 'Oi filha..',
    texto: 'Feliz aniversário filha 🎂🎁🎈TE AMO muitão',
    autor: 'Marcos Fernando',
  }

  public elen_dormindo = {
    imagem: 'elen-dormindo.jpg',
    titulo: '',
    texto: 'Elenzinha puxando um ronco',
    autor: 'Guilherme Albertini',
  }

  public guilherme_dormindo = {
    imagem: 'guilherme-dormindo.jpg',
    titulo: '',
    texto: 'E pra variar, eu puxando um ronco também',
    autor: 'Guilherme Albertini',
  }

  // 
  public carnaval_gui = {
    imagem: 'carnaval.jpg',
    titulo: '',
    texto: 'Uma fotinha no carnaval, que não pode faltar!',
    autor: 'Guilherme Albertini',
  }

  public carnaval_babi = {
    imagem: 'carnaval-babi.jpg',
    titulo: '',
    texto: 'Uma do carnaval comigo também né!',
    autor: 'Babi Viotto',
  }

  public golden_hour = {
    imagem: 'golden-hour.jpg',
    titulo: '',
    texto: 'Aguardando o sol nos ajudar a tirar uma fotinha estilo golden hour',
    autor: 'Babi Viotto',
  }

  public role_fail = {
    imagem: 'role-fail.jpg',
    titulo: '',
    texto: 'Dia que nosso rolê deu todo errado KKKKKKKKKK',
    autor: 'Guilherme Albertini e Babi Viotto',
  }

  public isa_babi = {
    imagem: 'isa-babi.jpg',
    titulo: '',
    texto: 'Tomando um pouquinho de alcóol.',
    autor: 'Belão e Babi Viotto',
  }

  public remember = {
    imagem: 'niver-18.jpg',
    titulo: '',
    texto: '2 anos já hein!!',
    autor: 'Galera toda',
  }

  public halloween = {
    imagem: 'halloween.jpg',
    titulo: '',
    texto: 'Curtindo um halloween de princesas acabadas.',
    autor: 'Babi Viotto',
  }

  public dentes = {
    imagem: 'dentes.jpg',
    titulo: '',
    texto: 'Mantendo a higiene bucal em dupla, sempre importante.',
    autor: 'Guilherme Albertini',
  }

  public leque = {
    imagem: 'leque.jpg',
    titulo: '',
    texto: 'Relaxando no calor de 72º de Indaiatuba',
    autor: 'Babi Viotto e Guilherme Albertini',
  }

  public pitanga = {
    imagem: 'pitanga.jpg',
    titulo: '',
    texto: 'Que saudade desse dia, 27 potinhos de sorvete cada um e já era.',
    autor: 'Guilherme Albertini',
  }

  public felipe_welida = {
    imagem: 'felipe-welida.jpeg',
    titulo: 'Oi Elenn!',
    texto: 'Desejamos muitas felicidades pra vc nesse dia especial, que Deus lhe proteja a cada passo em sua vida, e lhe guia sempre para os melhores caminhos. Aproveite seu dia.',
    autor: 'Felipe e Welida',
  }

  public thomaz = {
    imagem: 'thomaz.jpeg',
    titulo: 'Elen',
    texto: 'Feliz aniversário 🎂🎈🎈🎉',
    autor: 'Thomáz',
  }

  ngOnInit() {
    this.backgrounds_fundo = ["#cfe1e5", "#fbd1bb", "#f6d6d9", "#eeeeb2", "#ded2de"]
    this.onResize();
    this.verificaMobile();
    setTimeout(() => {
      $(".unicornio").addClass("opacidade-0");
      this.isLoaded = true;
      setTimeout(() => {
        $(".div-container").show();
        this.proxMusica();
        document.getElementById("audio").onended = () => {
          this.proxMusica();
        };
        this.controlaMusica();
      }, 1400)
    }, 4000)


  }

  onResize() {
    $(window).resize(function () {
      this.verificaMobile();
    })
  }

  verificaMobile() {
    if ($(window).width() < 1000) {
      this.isMobile = true;
      setTimeout(() => {
        $(".div-mensagens").css("background-repeat", "round");
        $(".div-mensagens").css("background-image", `url("../../assets/imagens/background.png")`);
      }, 4100)
      // }, 100)
    }
    else {
      this.isMobile = false;
      setTimeout(() => {
        $(".div-mensagens").css("background-repeat", "no-repeat");
        $(".div-mensagens").css("background-image", `url("../../assets/imagens/backgroundpc.png")`);
      }, 4100)
      // }, 100)
    }
  }

  popUnicornio(elemento) {
    $(elemento).addClass('pop-unicornio');
    let random = Math.floor(Math.random() * this.backgrounds_fundo.length)
    $(".fundo").css("background", this.backgrounds_fundo[random]);
    $(".audio span").css("color", this.backgrounds_fundo[random]);
    this.backgrounds_fundo.splice(random, 1);
    setTimeout(() => {
      $(".fundo").css("background", "var(--verde-agua)");
      $(".audio span").css("color", "var(--verde-agua)");
    }, 400);
    setTimeout(() => {
      $(elemento).hide();
    }, 400);
  }

  controlaMusica() {
    let estado = $("span.material-icons:nth-child(2)").html();
    setTimeout(() => {
      if (estado == "play_circle_filled".trim()) {
        document.getElementsByTagName("audio")[0].play().then(() => {
          $("span.material-icons:nth-child(2)").html("pause_circle_filled");
          $(".span-audio").css("opacity", "0");
        }).catch(e => {
          $(".span-audio").css("opacity", "100%");
        });
      }
      else {
        document.getElementsByTagName("audio")[0].pause();
        $("span.material-icons:nth-child(2)").html("play_circle_filled")
      }
    }, 200)
  }

  proxMusica() {
    let random = Math.floor(Math.random() * this.musicas.length)
    this.musica = this.musicas[random];
    document.getElementsByTagName("audio")[0].src = "../../assets/musicas/" + this.musica + ".mp3";
    this.musicas.splice(random, 1);
    if(this.musicas.length == 0)
      this.musicas = ["best-day", "bring-it", "watermelon-sugar", "gone", "ho-hey", "home", "lights-up"];
    document.getElementsByTagName("audio")[0].play();
  }

}
