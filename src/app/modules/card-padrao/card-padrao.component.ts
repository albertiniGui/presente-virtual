import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'card-padrao',
  templateUrl: './card-padrao.component.html',
  styleUrls: ['./card-padrao.component.css' , '../../mensagens/mensagens.component.css']
})
export class CardPadraoComponent implements OnInit {

  constructor() { }

  @Input() card;
  @Input() style;

  ngOnInit() {
  }

}
