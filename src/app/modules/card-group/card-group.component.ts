import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'card-group',
  templateUrl: './card-group.component.html',
  styleUrls: ['./card-group.component.css' , '../../mensagens/mensagens.component.css']
})
export class CardGroupComponent implements OnInit {

  constructor() { }

  @Input() cards: Array<any>;

  ngOnInit() {
  }

}
