import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'card-grande-cima',
  templateUrl: './card-grande-cima.component.html',
  styleUrls: ['./card-grande-cima.component.css' , '../../mensagens/mensagens.component.css']
})
export class CardGrandeCimaComponent implements OnInit {

  constructor() { }

  @Input() card;

  ngOnInit() {
  }

}
