import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'card-grande-baixo',
  templateUrl: './card-grande-baixo.component.html',
  styleUrls: ['./card-grande-baixo.component.css' , '../../mensagens/mensagens.component.css']
})
export class CardGrandeBaixoComponent implements OnInit {

  constructor() { }

  @Input() card;

  ngOnInit() {
  }

}
