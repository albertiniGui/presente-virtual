import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'card-horizontal',
  templateUrl: './card-horizontal.component.html',
  styleUrls: ['./card-horizontal.component.css' , '../../mensagens/mensagens.component.css']
})
export class CardHorizontalComponent implements OnInit {

  constructor() { }

  @Input() card;

  ngOnInit() {
  }

}
